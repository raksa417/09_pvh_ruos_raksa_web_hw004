import { Component } from "react";
import "flowbite";
import ageIcon from "../assets/age-icon.png";
import TableComponent from "./TableComponent";

export default class InputComponent extends Component {
  constructor() {
    super();
    this.state = {
      person: [
        {
          id: 1,
          email: "dara@gmail.com",
          userName: "dara",
          age: 20,
          status: "Pending",
        },
        {
          id: 2,
          email: "kaka@gmail.com",
          userName: "kaka",
          age: 30,
          status: "Pending",
        },
        {
          id: 3,
          email: "sopheak@gmail.com",
          userName: "sopheak",
          age: 25,
          status: "Pending",
        },
      ],
    };
  }

  register = () => {
        let e =  document.getElementById("email").value;
        let u = document.getElementById("username").value;
        let a = document.getElementById("age").value;

        if(e === ""){
            e = "null";
        }
        if(u === ""){
            u = "null";
        }
        if(a === ""){
           a = "null";
        }

        const newObj = {
            id: this.state.person.length+1, 
            email: e,
            userName: u,
            age: a,
            status: "Pending",
        }
   
        this.setState({
            person: [...this.state.person,newObj]
        })
  }

  receiveStatus = (data) =>{
      const obj = this.state.person;
      for(let i = 0; i < obj.length; i++){
        if(obj[i].id === data.id){
          data.status = (data.status === "Pending" ? "Done" : "Pending");
      }
      this.setState({...this.state.person})
  }};

  render() {
    return (
      <div className="m-auto w-[850px] mt-16">
        <h1 className="text-5xl mb-12 text-center font-extrabold">
          <span className="text-transparent bg-clip-text bg-gradient-to-r from-purple-400 to-pink-600">
             Please fill your
          </span>
          <span> information</span>
        </h1>

        {/* Email */}
        <label
          for="email"
          className="block mb-2 font-medium text-gray-900 dark:text-white text-xl"
        >
          Email
        </label>
        <div className="flex mb-6">
          <span className="inline-flex items-center px-3 text-lg text-gray-900 bg-gray-200 border border-r-0 border-gray-300 rounded-l-md dark:bg-gray-600 dark:text-gray-400 dark:border-gray-600">
            <svg
              aria-hidden="true"
              className="w-5 h-5 text-gray-500 dark:text-gray-400"
              fill="currentColor"
              viewBox="0 0 20 20"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path d="M2.003 5.884L10 9.882l7.997-3.998A2 2 0 0016 4H4a2 2 0 00-1.997 1.884z"></path>
              <path d="M18 8.118l-8 4-8-4V14a2 2 0 002 2h12a2 2 0 002-2V8.118z"></path>
            </svg>
          </span>

          <input
            type="text"
            id="email"
            className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-r-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
            placeholder="raksaruos@gmail.com"
          />
        </div>

        {/* UserName */}
        <label
          for="username"
          className="block mb-2 font-medium text-gray-900 dark:text-white text-xl"
        >
          Username
        </label>
        <div class="flex mb-6">
          <span class="inline-flex items-center px-3 text-lg text-gray-900 bg-gray-200 border border-r-0 border-gray-300 rounded-l-md dark:bg-gray-600 dark:text-gray-400 dark:border-gray-600">
            @
          </span>
          <input
            type="text"
            id="username"
            className="rounded-none rounded-r-lg bg-gray-50 border text-gray-900 focus:ring-blue-500 focus:border-blue-500 block flex-1 min-w-0 w-full text-sm border-gray-300 p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
            placeholder="raksaruos"
          />
        </div>

        {/* Age */}
        <label
          for="age"
          className="block mb-2 font-medium text-gray-900 dark:text-white text-xl"
        >
          Age
        </label>
        <div className="flex mb-8">
          <span className="inline-flex items-center px-3 text-lg text-gray-900 bg-gray-200 border border-r-0 border-gray-300 rounded-l-md dark:bg-gray-600 dark:text-gray-400 dark:border-gray-600">
            <img src={ageIcon} alt="ageIcon" style={{ width: "20px" }} />
          </span>

          <input
            type="number"
            id="age"
            className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-r-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
            placeholder="20"
          />
        </div>

        {/* Button */}
        <div class="flex justify-end">
          <button
            type="button"
            className="text-white bg-blue-500 hover:bg-blue-600 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-[17px] px-20 py-2.5 mr-2 mb-2 dark:bg-blue-600 dark:hover:bg-blue-700 focus:outline-none dark:focus:ring-blue-800"
            onClick={this.register}
          >
            Register
          </button>
        </div>

        {/* Table for display information */}
         <TableComponent data = {this.state.person} getStatus={this.receiveStatus}/>
      </div>
    );
  }
}
